import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Query,
  Delete,
} from '@nestjs/common';
import { ExpensesService } from './expenses.service';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';

@Controller('expenses')
export class ExpensesController {
  constructor(private readonly expensesService: ExpensesService) {}

  @Post()
  create(@Body() createExpenseDto: CreateExpenseDto) {
    return this.expensesService.create(createExpenseDto);
  }

  @Get(':userId')
  findAll(
    @Param('userId') userId: number,
    @Query()
    query: { from?: Date | null; to?: Date | null; categories?: string },
  ) {
    const fromDate = new Date(query.from);
    const toDate = new Date(query.to);

    return this.expensesService.findAll(
      Number(userId),
      query.categories &&
        query.categories.split(',').map((item) => parseInt(item, 10)),
      fromDate.getTime() ? fromDate : null,
      toDate.getTime() ? toDate : null,
    );
  }

  @Put()
  update(@Body() body: { finance: UpdateExpenseDto; userId: number }) {
    return this.expensesService.update(body);
  }

  @Delete()
  remove(@Body() body: { financeId: number; userId: number }) {
    return this.expensesService.remove(body);
  }
}
