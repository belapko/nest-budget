import { Injectable } from '@nestjs/common';
import { CreateExpenseDto } from './dto/create-expense.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class ExpensesService {
  constructor(private prisma: PrismaService) {}

  async create(createExpenseDto: CreateExpenseDto) {
    const createdExpense = await this.prisma.expense.create({
      data: createExpenseDto,
      include: { category: true },
    });
    return createdExpense;
  }

  async findAll(
    userId: number,
    categoriesIds?: number[],
    fromDate?: Date | null,
    toDate?: Date | null,
  ) {
    // const delay = new Promise((resolve, reject) => {
    //   setTimeout(resolve, 5000);
    // });
    // await delay;
    try {
      const isBetweenDates = toDate && fromDate;

      const expenses = await this.prisma.expense.findMany({
        where: {
          userId,
          date: {
            lte: isBetweenDates
              ? toDate.toISOString() === fromDate.toISOString()
                ? new Date(toDate.setDate(toDate.getDate() + 1))
                : toDate
              : undefined,
            gte: isBetweenDates ? fromDate : undefined,
          },
          categoryId: { in: categoriesIds || undefined },
        },
        orderBy: { date: 'desc' },
        include: { category: true },
      });
      // Массив объектов, где ключ - дата, значение - массив расходов
      const groupedByDate = expenses.reduce((acc, item) => {
        // Преобразование даты в формат "гггг-мм-дд"
        const dateKey = new Date(item.date).toISOString().split('T')[0];

        // Если ключа с текущей датой еще нет в аккумуляторе, создаем его
        if (!acc[dateKey]) {
          acc[dateKey] = [];
        }

        // Добавляем текущий объект в массив для текущей даты
        acc[dateKey].push(item);

        return acc;
      }, {});
      return groupedByDate;
    } catch (e) {
      return e;
    }
  }

  async update({ finance, userId }) {
    try {
      const updatedExpense = await this.prisma.expense.update({
        where: { id: finance.id, userId },
        data: {
          categoryId: finance.categoryId,
          amount: Number(finance.amount).toFixed(2),
          date: finance.date,
          title: finance.title,
        },
        include: { category: true },
      });
      return updatedExpense;
    } catch (e) {
      return e;
    }
  }

  async remove({ financeId, userId }) {
    try {
      await this.prisma.expense.delete({ where: { id: financeId, userId } });
      return;
    } catch (e) {
      return e;
    }
  }
}
