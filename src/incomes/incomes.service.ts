import { Injectable } from '@nestjs/common';
import { CreateIncomeDto } from './dto/create-income.dto';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class IncomesService {
  constructor(private prisma: PrismaService) {}

  async create(createIncomeDto: CreateIncomeDto) {
    const createdIncome = await this.prisma.income.create({
      data: createIncomeDto,
    });
    return createdIncome;
  }

  async findAll(userId: number, fromDate?: Date | null, toDate?: Date | null) {
    try {
      const isBetweenDates = toDate && fromDate;

      const incomes = await this.prisma.income.findMany({
        where: {
          userId,
          date: {
            lte: isBetweenDates
              ? toDate.toISOString() === fromDate.toISOString()
                ? new Date(toDate.setDate(toDate.getDate() + 1))
                : toDate
              : undefined,
            gte: isBetweenDates ? fromDate : undefined,
          },
        },
        orderBy: { date: 'desc' },
      });
      // Массив объектов, где ключ - дата, значение - массив расходов
      const groupedByDate = incomes.reduce((acc, item) => {
        // Преобразование даты в формат "гггг-мм-дд"
        const dateKey = new Date(item.date).toISOString().split('T')[0];

        // Если ключа с текущей датой еще нет в аккумуляторе, создаем его
        if (!acc[dateKey]) {
          acc[dateKey] = [];
        }

        // Добавляем текущий объект в массив для текущей даты
        acc[dateKey].push(item);

        return acc;
      }, {});
      return groupedByDate;
    } catch (e) {
      return e;
    }
  }

  async update({ finance, userId }) {
    try {
      const updatedIncome = await this.prisma.income.update({
        where: { id: finance.id, userId },
        data: {
          amount: Number(finance.amount).toFixed(2),
          date: finance.date,
          title: finance.title,
        },
      });
      return updatedIncome;
    } catch (e) {
      return e;
    }
  }

  async remove({ financeId, userId }) {
    try {
      await this.prisma.income.delete({ where: { id: financeId, userId } });
      return;
    } catch (e) {
      return e;
    }
  }
}
