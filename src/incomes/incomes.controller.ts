import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { IncomesService } from './incomes.service';
import { CreateIncomeDto } from './dto/create-income.dto';
import { UpdateIncomeDto } from './dto/update-income.dto';

@Controller('incomes')
export class IncomesController {
  constructor(private readonly incomesService: IncomesService) {}

  @Post()
  create(@Body() createIncomeDto: CreateIncomeDto) {
    return this.incomesService.create(createIncomeDto);
  }

  @Get(':userId')
  findAll(
    @Param('userId') userId: number,
    @Query()
    query: { from?: Date | null; to?: Date | null },
  ) {
    const fromDate = new Date(query.from);
    const toDate = new Date(query.to);

    return this.incomesService.findAll(
      Number(userId),
      fromDate.getTime() ? fromDate : null,
      toDate.getTime() ? toDate : null,
    );
  }

  @Put()
  update(@Body() body: { finance: UpdateIncomeDto; userId: number }) {
    return this.incomesService.update(body);
  }

  @Delete()
  remove(@Body() body: { financeId: number; userId: number }) {
    return this.incomesService.remove(body);
  }
}
