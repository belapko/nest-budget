import { ApiProperty } from '@nestjs/swagger';

export class AuthUserDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;

  isTest?: boolean;
}

export class UpdateUserDto extends AuthUserDto {
  id: string;
}
