import { Injectable } from '@nestjs/common';
import { AuthUserDto, UpdateUserDto } from './dto/create-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { hash } from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}
  async create(user: AuthUserDto) {
    const hashedPassword = await hash(user.password, 10);
    user.password = hashedPassword;
    return await this.prisma.user.create({ data: user });
  }

  async update(user: UpdateUserDto) {
    const hashedPassword = await hash(user.password, 10);
    const dateToUpdate = {
      email: user.email,
      password: hashedPassword,
      isTest: false,
    };
    return await this.prisma.user.update({
      where: { id: Number(user.id) },
      data: dateToUpdate,
    });
  }

  async findById(id: number) {
    return await this.prisma.user.findUnique({ where: { id } });
  }

  async findByEmail(email: string) {
    return await this.prisma.user.findUnique({ where: { email } });
  }

  async count() {
    return await this.prisma.user.count();
  }

  async remove(id: number) {
    await this.prisma.expense.deleteMany({ where: { userId: id } });
    return await this.prisma.user.delete({ where: { id } });
  }
}
