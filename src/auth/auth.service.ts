import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthUserDto, UpdateUserDto } from 'src/users/dto/create-user.dto';
import { UsersService } from 'src/users/users.service';
import { compare } from 'bcrypt';

function exclude(user, keys) {
  return Object.fromEntries(
    Object.entries(user).filter(([key]) => !keys.includes(key)),
  );
}

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(user: AuthUserDto, res) {
    const existingUser = await this.usersService.findByEmail(user.email);
    if (!existingUser) {
      throw new UnauthorizedException('User not exists');
    }

    const passwordsCompared = await compare(
      user.password,
      existingUser.password,
    );

    if (!passwordsCompared) {
      throw new UnauthorizedException('Wrong password');
    }

    const payload = {
      id: existingUser.id,
      email: existingUser.email,
    };

    res.cookie(
      'refresh_token',
      this.jwtService.sign(payload, { expiresIn: '7d' }),
    );

    const userWithoutPassword = exclude(existingUser, 'password');

    return {
      user: userWithoutPassword,
      accessToken: this.jwtService.sign(payload), // refreshToken: this.jwtService.sign(payload, { expiresIn: '7d' }),
    };
  }

  async signUp(user: AuthUserDto | UpdateUserDto, res) {
    const existingUser = await this.usersService.findByEmail(user.email);
    if (existingUser) {
      throw new BadRequestException('User already exists');
    }

    let createdUser;
    if (user.isTest) {
      createdUser = await this.usersService.update(user as UpdateUserDto);
    } else {
      createdUser = await this.usersService.create(user);
    }

    const payload = {
      id: createdUser.id,
      email: createdUser.email,
    };

    res.cookie(
      'refresh_token',
      this.jwtService.sign(payload, { expiresIn: '7d' }),
    );

    const userWithoutPassword = exclude(createdUser, 'password');

    return {
      user: userWithoutPassword,
      accessToken: this.jwtService.sign(payload),
    };
  }

  async refreshToken(payload: { id: number; email: string }) {
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async test(res) {
    const usersCount = await this.usersService.count();
    const user: AuthUserDto = {
      email: `anonymous${usersCount + 1}@test.com`,
      password: Math.random().toString(36).slice(-8),
      isTest: true,
    };
    const createdUser = await this.usersService.create(user);

    const payload = {
      id: createdUser.id,
      email: createdUser.email,
    };

    res.cookie(
      'refresh_token',
      this.jwtService.sign(payload, { expiresIn: '7d' }),
    );

    const userWithoutPassword = exclude(createdUser, 'password');

    return {
      user: userWithoutPassword,
      accessToken: this.jwtService.sign(payload),
    };
  }

  async remove(id: string) {
    await this.usersService.remove(Number(id));
    return;
  }
}
