import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ExpensesModule } from './expenses/expenses.module';
import { CategoriesModule } from './categories/categories.module';
import { IncomesModule } from './incomes/incomes.module';

@Module({
  imports: [PrismaModule, UsersModule, AuthModule, ExpensesModule, CategoriesModule, IncomesModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
